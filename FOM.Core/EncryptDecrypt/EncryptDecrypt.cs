﻿
using System;
using System.Configuration;
using System.Security.Cryptography;
using System.Text;

namespace FOM.Core
{
    public class EncryptDecrypt  
    {

        private static readonly Lazy<EncryptDecrypt> lazy = new Lazy<EncryptDecrypt>(() => new EncryptDecrypt());
        public static EncryptDecrypt Instance { get { return lazy.Value; } }

        /// <summary>
        /// Global value , get Value by web.Config
        /// </summary>
        private string key { get { return null; } }// Convert.ToString(WebConfigResource.EncKey); } }

        /// <summary>
        /// Text Convert into Encrypt Value
        /// </summary>
        /// <param name="data">Text Value</param>
        /// <returns></returns>
        public string Encrypt(string data)
        {
            try
            {
                RijndaelManaged rijndaelCipher = new RijndaelManaged();
                rijndaelCipher.Mode = CipherMode.CBC; //remember this parameter
                rijndaelCipher.Padding = PaddingMode.PKCS7; //remember this parameter

                rijndaelCipher.KeySize = 0x80;
                rijndaelCipher.BlockSize = 0x80;
                byte[] pwdBytes = Encoding.UTF8.GetBytes(key);
                byte[] keyBytes = new byte[0x10];
                int len = pwdBytes.Length;

                if (len > keyBytes.Length)
                {
                    len = keyBytes.Length;
                }

                Array.Copy(pwdBytes, keyBytes, len);
                rijndaelCipher.Key = keyBytes;
                rijndaelCipher.IV = keyBytes;
                ICryptoTransform transform = rijndaelCipher.CreateEncryptor();
                byte[] plainText = Encoding.UTF8.GetBytes(data);

                return Convert.ToBase64String
                (transform.TransformFinalBlock(plainText, 0, plainText.Length)).Replace("/", ",,").Replace("+", "~");
                //return HttpUtility.UrlEncode(Convert.ToBase64String(transform.TransformFinalBlock(plainText, 0, plainText.Length)));
            }
            catch
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// Encrypt value Convert into Actual text
        /// </summary>
        /// <param name="data">Encrypt value</param>
        /// <returns></returns>
        public string Decrypt(string data)
        {
            try
            {
                //data = HttpUtility.UrlDecode(data);
                data = data.Replace(",,", "/").Replace("~", "+");
                RijndaelManaged rijndaelCipher = new RijndaelManaged();
                rijndaelCipher.Mode = CipherMode.CBC;
                rijndaelCipher.Padding = PaddingMode.PKCS7;

                rijndaelCipher.KeySize = 0x80;
                rijndaelCipher.BlockSize = 0x80;
                byte[] encryptedData = Convert.FromBase64String(data);
                byte[] pwdBytes = Encoding.UTF8.GetBytes(key);
                byte[] keyBytes = new byte[0x10];
                int len = pwdBytes.Length;

                if (len > keyBytes.Length)
                {
                    len = keyBytes.Length;
                }

                Array.Copy(pwdBytes, keyBytes, len);
                rijndaelCipher.Key = keyBytes;
                rijndaelCipher.IV = keyBytes;
                byte[] plainText = rijndaelCipher.CreateDecryptor().TransformFinalBlock
                            (encryptedData, 0, encryptedData.Length);

                return Encoding.UTF8.GetString(plainText);
            }
            catch
            {
                return string.Empty;
            }
        }
    }


}
