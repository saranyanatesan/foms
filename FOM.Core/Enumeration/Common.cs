﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JKApi.Business.Enumeration
{
    public enum UserType
    {
        [Description("ADMIN")]
        ADMIN = 1,

        [Description("USER")]
        USER,

        [Description("CSR")]
        CSR,

        [Description("VIEWER")]
        VIEWER
    }

    public enum AddressType
    {
        [Description("SHIPPING")]
        SHIPPING = 1,

        [Description("BILLING")]
        BILLING,
    }
    public enum SearchIn
    {

        [Description("Name")]
        Name = 0,
        [Description("Number")]
        Number = 1,
        [Description("Address")]
        Address = 3,
        [Description("City")]
        City = 4,
        [Description("ZipCode")]
        ZipCode = 5,

        [Description("Account Type")]
        AccountType = 7,
        [Description("Contract Amount")]
        ContractAmount = 8,
        [Description("Contact")]
        Contact = 9,
        [Description("Operations Mgr.")]
        OperationsMgr = 10

    }

    //Filterby for Trasactions
    public enum FilterBy
    {
        [Description("None")]
        None = 1,
        [Description("Available for Revenue Adjustment Only")]
        AvailableforRevenueAdjustmentOnly = 2,
        [Description("Bill Run Transactions Only")]
        BillRunTransactionsOnly = 4,
        [Description("Manual Transactions Only")]
        ManualTransactionsOnly = 5,

    }

    public enum SearchBy
    {
        [Description("Select One")]
        SelectOne = 0,
        [Description("Customer Name")]
        CustomerName = 7,
        [Description("Customer Number")]
        CustomerNumber = 5,
        [Description("Invoice Number")]
        InvoiceNumber = 2,
        [Description("Franchisee Number")]
        FranchiseeNumber = 10,

    }

    public enum TypeList
    {
        Customer = 1,
        Franachisee = 2
    }

    public enum ContactTypeList
    {
        Main = 1,
        Billing = 2,
        [Description("Physical Location")]
        PhysicalLocation = 3, 
        Contact = 4
    }

    public enum ButtonType
    {
        Save = 1,
        Continue = 2, 
        Back = 3,
        Cancel = 4
    }

}
