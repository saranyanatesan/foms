﻿
using FOM.ViewModels;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace FOM.Core
{
    public class ClaimView
    {
        private static readonly Lazy<ClaimView> lazy = new Lazy<ClaimView>(() => new ClaimView());
        public static ClaimView Instance { get { return lazy.Value; } }

        public string GetCLAIM_USERNAME()
        {
            //Get the user's claims
            var currentPrincipalClaims = ((ClaimsIdentity)Thread.CurrentPrincipal.Identity).Claims;

            //If there are claims present, then get the type of Company
            if (currentPrincipalClaims != null && currentPrincipalClaims.Count() > 0)
            {
                return currentPrincipalClaims.Where(clm => clm.Type.ToLower() == ClaimTypeName.CLAIM_USERNAME.ToLower()).Select(clm => clm.Value).FirstOrDefault();
            }
            else
            {
                return string.Empty;
            }
        }

        public string GetCLAIM_USERID()
        {
            //Get the user's claims
            var currentPrincipalClaims = ((ClaimsIdentity)Thread.CurrentPrincipal.Identity).Claims;

            //If there are claims present, then get the type of Company
            if (currentPrincipalClaims != null && currentPrincipalClaims.Count() > 0)
            {
                return currentPrincipalClaims.Where(clm => clm.Type.ToLower() == ClaimTypeName.CLAIM_USERID.ToLower()).Select(clm => clm.Value).FirstOrDefault();
            }
            else
            {
                return string.Empty;
            }
        }

        public string GetCLAIM_ROLE_TYPE()
        {
            //Get the user's claims
            var currentPrincipalClaims = ((ClaimsIdentity)Thread.CurrentPrincipal.Identity).Claims;

            //If there are claims present, then get the type of Company
            if (currentPrincipalClaims != null && currentPrincipalClaims.Count() > 0)
            {
                return currentPrincipalClaims.Where(clm => clm.Type.ToLower() == ClaimTypeName.CLAIM_ROLE_TYPE.ToLower()).Select(clm => clm.Value).FirstOrDefault();
            }
            else
            {
                return string.Empty;
            }
        }

        public string GetCLAIM_Role()
        {
            //Get the user's claims
            var currentPrincipalClaims = ((ClaimsIdentity)Thread.CurrentPrincipal.Identity).Claims;

            //If there are claims present, then get the type of Company
            if (currentPrincipalClaims != null && currentPrincipalClaims.Count() > 0)
            {
                return currentPrincipalClaims.Where(clm => clm.Type.ToLower() == ClaimTypes.Role.ToLower()).Select(clm => clm.Value).FirstOrDefault();
            }
            else
            {
                return string.Empty;
            }
        }
        
        public UserViewModel GetCLAIM_PERSON_INFORMATION()
        {
            //Get the user's claims
            var currentPrincipalClaims = ((ClaimsIdentity)Thread.CurrentPrincipal.Identity).Claims;

            //If there are claims present, then get the type of Company
            if (currentPrincipalClaims != null && currentPrincipalClaims.Count() > 0)
            {
                return JsonConvert.DeserializeObject<UserViewModel>(currentPrincipalClaims.Where(clm => clm.Type.ToLower() == ClaimTypeName.CLAIM_PERSON_INFORMATION.ToLower()).Select(clm => clm.Value).FirstOrDefault());
            }
            else
            {
                return null;
            }
        }

        public int GetCLAIM_SELECTED_REGION_ID()
        {
            //Get the user's claims
            var currentPrincipalClaims = ((ClaimsIdentity)Thread.CurrentPrincipal.Identity).Claims;

            //If there are claims present, then get the type of Company
            if (currentPrincipalClaims != null && currentPrincipalClaims.Count() > 0)
            {
                return Convert.ToInt32(currentPrincipalClaims.Where(clm => clm.Type.ToLower() == ClaimTypeName.CLAIM_SELECTED_REGION_ID.ToLower()).Select(clm => clm.Value).FirstOrDefault());
            }
            else
            {
                return 0;
            }
        }


    }
}
