﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace FOM.WebAPI.Models.Account
{
    /// <summary>
    /// Request model for Login
    /// </summary>
    public class LoginRequestModel : IModelBase
    {
        /// <summary>
        /// Username
        /// </summary>
        [Required]
        [JsonProperty(PropertyName = "username")]
        public string Username { get; set; }

        /// <summary>
        /// Password
        /// </summary>
        [Required]
        [JsonProperty(PropertyName = "password")]
        public string Password { get; set; }

        /// <summary>
        /// Constructor to fill dummy data
        /// </summary>
        /// <param name="fillDummyData"></param>
        public LoginRequestModel(bool fillDummyData)
        {
            Username = "Default";
            Password = "ldr2435jk";
        }

        /// <summary>
        /// Empty contructor
        /// </summary>
        public LoginRequestModel()
        {

        }
    }

    /// <summary>
    /// Response model for Login
    /// </summary>
    public class LoginResponseModel : IModelBase
    {
        /// <summary>
        /// Username
        /// </summary>
        [JsonProperty(PropertyName = "username")]
        public string Username { get; set; }

        /// <summary>
        /// User ID
        /// </summary>
        [JsonProperty(PropertyName = "id")]
        public int Id { get; set; }

        /// <summary>
        /// API Key
        /// </summary>
        [JsonProperty(PropertyName = "api_key")]
        public string Key { get; set; }
    }
}