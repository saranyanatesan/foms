﻿using Elmah;
using FOM.Core;
using FOM.Service;
using Microsoft.Practices.Unity;
using Microsoft.Practices.Unity.InterceptionExtension;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace FOM.MVC
{
    public static class Bootstrapper
    {
        public static void Initialise()
        {
            var container = new UnityContainer();

            IControllerFactory unityFactory = new UnityControllerFactory(container);
            ControllerBuilder.Current.SetControllerFactory(unityFactory);

            BuildUnityContainer(container);

        }

        private static void BuildUnityContainer(IUnityContainer container)
        {   
            container.RegisterType<IJobService, JobService>(new Interceptor<InterfaceInterceptor>());
            container.RegisterType<IScheduleService, ScheduleService>(new Interceptor<InterfaceInterceptor>());
            container.RegisterType<ICustomerService, CustomerService>(new Interceptor<InterfaceInterceptor>());
            container.RegisterType<IEmployeeService, EmployeeService>(new Interceptor<InterfaceInterceptor>());
            container.RegisterType<IMapService, MapService>(new Interceptor<InterfaceInterceptor>());
            container.RegisterType<IReportService, ReportService>(new Interceptor<InterfaceInterceptor>());
            container.RegisterType<IOtherService, OtherService>(new Interceptor<InterfaceInterceptor>());
            container.RegisterType<IDashboardService, DashboardService>(new Interceptor<InterfaceInterceptor>());

        }
    }

    /// <summary>
    /// Custom Controller Factory to allow Unity to manage controller creation and dep injection.
    /// </summary>
    public class UnityControllerFactory : DefaultControllerFactory
    {
        private readonly IUnityContainer _container;

        public UnityControllerFactory(IUnityContainer container)
        {
            _container = container;

            _container.AddNewExtension<Interception>();
        }

        public override IController CreateController(RequestContext requestContext, string controllerName)
        {

            try
            {
                return (IController)_container.Resolve(GetControllerType(requestContext, controllerName));
            }
            catch (Exception e)
            {
                ErrorSignal.FromCurrentContext().Raise(e);
            }
            return base.CreateController(requestContext, controllerName);
        }

        public override void ReleaseController(IController controller)
        {
            _container.Teardown(controller);

            base.ReleaseController(controller);
        }
    }
}