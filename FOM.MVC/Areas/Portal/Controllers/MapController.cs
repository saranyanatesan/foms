﻿using MvcBreadCrumbs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FOM.ViewModels;

namespace FOM.MVC.Areas.Portal.Controllers
{
    public class MapController : ViewControllerBase
    {

        public MapController()
        {
            ViewBag.HMenu = VMenuEnum.Map.ToString();
        }

        // GET: Portal/Map
        public ActionResult Index()
        {
            BreadCrumb.Add(Url.Action("Index", "Map", new { area = "Portal" }), "Map");
            return View();
        }
    }
}