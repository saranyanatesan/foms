﻿using MvcBreadCrumbs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FOM.ViewModels;

namespace FOM.MVC.Areas.Portal.Controllers
{
    public class ScheduleController : ViewControllerBase
    {

        public ScheduleController()
        {
            ViewBag.HMenu = VMenuEnum.Schedule.ToString();
        }

        // GET: Portal/Schedule
        public ActionResult Index()
        {
            BreadCrumb.Add(Url.Action("Index", "Schedule", new { area = "Portal" }), "Schedule");
            return View();
        }
    }
}