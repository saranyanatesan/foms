﻿using MvcBreadCrumbs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FOM.ViewModels;

namespace FOM.MVC.Areas.Portal.Controllers
{
    public class ReportController : ViewControllerBase
    {

        public ReportController()
        {
            ViewBag.HMenu = VMenuEnum.Report.ToString();
        }

        // GET: Portal/Report
        public ActionResult Index()
        {
            BreadCrumb.Add(Url.Action("Index", "Report", new { area = "Portal" }), "Report");
            return View();
        }
    }
}