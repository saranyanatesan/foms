﻿using MvcBreadCrumbs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FOM.ViewModels;

namespace FOM.MVC.Areas.Portal.Controllers
{
    [BreadCrumb(Clear = true, Label = "Portal", Order = 0)]
    [Compress]
    public class DashBoardController : ViewControllerBase
    {
        public DashBoardController()
        {
            ViewBag.HMenu = VMenuEnum.Dashboard.ToString();
        }
        // GET: Portal/DashBoard
        public ActionResult Index()
        { 
            BreadCrumb.Add(Url.Action("Index", "DashBoard", new { area = "Portal" }), "Dash Board");
            //return View(@"~\Areas\Portal\Views\DashBoard\Index.cshtml");
            return View();
        }
    }
}