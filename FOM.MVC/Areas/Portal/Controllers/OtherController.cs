﻿using MvcBreadCrumbs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FOM.ViewModels;

namespace FOM.MVC.Areas.Portal.Controllers
{
    public class OtherController : ViewControllerBase
    {

        public OtherController()
        {
            ViewBag.HMenu = VMenuEnum.Other.ToString();
        }

        // GET: Portal/Other
        public ActionResult Index()
        {
            BreadCrumb.Add(Url.Action("Index", "Other", new { area = "Portal" }), "Other");
            return View();
        }
    }
}