﻿using MvcBreadCrumbs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FOM.ViewModels;
using FOM.Service;
using FOM.Resources;

namespace FOM.MVC.Areas.Portal.Controllers
{
    public class CustomerController : ViewControllerBase
    {

        public CustomerController(ICustomerService CustomerService)
        {
            this._customerService = CustomerService;
            ViewBag.HMenu = VMenuEnum.Customer.ToString();
        }

        // GET: Portal/Customer
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Save()
        {
            return View();
        }

        #region Customer Listing

        //[MenuAccess]
        [HttpGet]
        public ActionResult CustomerListing()
        {
            ViewCustomerListModel viewCustomerListModel = new ViewCustomerListModel()
            {
                PageSize = 2,// Convert.ToInt32(WebConfigResource.ListPageSize),
                RegionFranchiseMappingId = 1// _claimView.GetCLAIM_SELECTED_REGION_ID()
            };

            viewCustomerListModel = _customerService.CustomerList(viewCustomerListModel);
            return View(viewCustomerListModel);
        }

        [HttpPost]
        public ActionResult CustomerListing(ViewCustomerListModel viewCustomerListModel)
        {
            viewCustomerListModel = _customerService.CustomerList(viewCustomerListModel);
            return PartialView("_CustomerList", viewCustomerListModel);
        }


        public ActionResult DeactivateCustomer(int Row)
        {
            ViewCustomerListModel viewCustomerListModel = new ViewCustomerListModel();
            return Json(new { Message = viewCustomerListModel.Message, MessageType = viewCustomerListModel.MessageType }, JsonRequestBehavior.AllowGet);
        }


        public ActionResult ActivateCustomer(int Row)
        {
            ViewCustomerListModel viewCustomerListModel = new ViewCustomerListModel();
            return Json(new { Message = viewCustomerListModel.Message, MessageType = viewCustomerListModel.MessageType }, JsonRequestBehavior.AllowGet);
        }


        #endregion
    }
}