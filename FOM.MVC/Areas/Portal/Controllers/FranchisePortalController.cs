﻿using MvcBreadCrumbs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FOM.ViewModels;


namespace FOM.MVC.Areas.Portal.Controllers
{
    public class FranchisePortalController : ViewControllerBase
    {

        public FranchisePortalController()
        {
            ViewBag.HMenu = VMenuEnum.FranchisePortal.ToString();
        }

        // GET: Portal/FranchisePortal
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Save()
        {
            return View();
        }

        public ActionResult List()
        {
            return View();
        }

        public ActionResult Customers()
        {
            return View();
        }

        public ActionResult Transaction()
        {
            return View();
        }

        public ActionResult Inspections()
        {
            return View();
        }

        public ActionResult CustomerService()
        {
            return View();
        }

        public ActionResult Collection()
        {
            return View();
        }
    }
}