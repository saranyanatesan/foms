﻿using MvcBreadCrumbs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FOM.ViewModels;
using FOM.Service;

namespace FOM.MVC.Areas.Portal.Controllers
{
    public class JobController : ViewControllerBase
    {

        public JobController(IJobService JobService)
        {
            this._jobService = JobService;
            ViewBag.HMenu = VMenuEnum.Job.ToString();
        }

        // GET: Portal/Job
        public ActionResult Index()
        {
            BreadCrumb.Add(Url.Action("Index", "Job", new { area = "Portal" }), "Job");
            return View();
        }

        #region Job Listing

        //[MenuAccess]
        [HttpGet]
        public ActionResult JobListing()
        {
            ViewJobListModel viewJobListModel = new ViewJobListModel()
            {
                PageSize = 2,// Convert.ToInt32(WebConfigResource.ListPageSize),
                RegionFranchiseMappingId = 1// _claimView.GetCLAIM_SELECTED_REGION_ID()
            };

            viewJobListModel = _jobService.JobList(viewJobListModel);
            return View(viewJobListModel);
        }

        [HttpPost]
        public ActionResult JobListing(ViewJobListModel viewJobListModel)
        {
            viewJobListModel = _jobService.JobList(viewJobListModel);
            return PartialView("_JobList", viewJobListModel);
        }
      


        #endregion

    }
}
