﻿using FOM.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FOM.MVC
{
    public class LoggingFilterAttribute : System.Web.Mvc.ActionFilterAttribute
    {
        NLogger nLogger = NLogger.Instance;

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            filterContext.HttpContext.Items["RequestTime"] = DateTime.Now;
            base.OnActionExecuting(filterContext);
        }

        public override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            filterContext.HttpContext.Items["ResponseTime"] = DateTime.Now;
            if (filterContext.Exception == null)
            {
               nLogger.InfoWeb(filterContext);
            }
        }
    }
}