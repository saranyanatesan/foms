/*---------------------------Custom Paging and sortings-----------------------------------------*/

function bindSort(obj, sortByID, sortOrderID, actionTypeID, fnCallback, PagePrefix) {    
    if (PagePrefix == undefined) {
        PagePrefix = "";
    }
    var sortby = ($(obj).attr("sortby"));
    var previouseSortBy = $("#" + PagePrefix + sortByID).val();
    var sortOrder = $("#" + PagePrefix + sortOrderID).val();
    if (sortby == previouseSortBy) {
        if (sortOrder == '0' || sortOrder=="DESC") {
            $("#" + PagePrefix + sortOrderID).val("ASC");
            $(obj).addClass("desc");
        }
        else {
            $("#" + PagePrefix + sortOrderID).val("DESC");
            $(obj).addClass("asc");
        }
        $("#" + PagePrefix + sortByID).val(sortby);
    }
    else {
        $("#" + PagePrefix + sortOrderID).val("ASC");
        $("#" + PagePrefix + sortByID).val(sortby);
    }
    $("#" + PagePrefix + actionTypeID).val("sortList");
    //$("#currentPage").val("1");
    fnCallback();

}



function createFooter(footerContainer, pagesizeContainer, totalPages, rowsPerPage, ActionTypeID, CurrentPageID,PageSizeID, fnCallback, PagePrefix) {
    //if (totalPages > 1)
    if (totalPages == totalPages) {

        var currentPage = $("#" + CurrentPageID).val();
        var pgSizeFooter = "<select class='rowsPerPage form-control'><option value='10'>10</option><option value='20'>20</option><option value='50'>50</option><option value='100'>100</option></select>"
        var footer = "<ul class='pagination pagination-sm'>";
        if (parseInt(currentPage) > 1) {
            footer = footer + "<li class='prev-btn'><a href='javascript:void(0)' title='Previous' class='page " + PagePrefix + "' id=" + (parseInt(currentPage) - 1) + "><i class='fa fa-chevron-left' aria-hidden='true'></i></a></li>"
        }

        // for (var i = 1; i < (parseInt(totalPages) + 1); i++) {
        var startIndex = 0;

        if (currentPage > 6)
            startIndex = currentPage - 6;

        for (var i = 1; i <= 11; i++) {

            if ((i + startIndex) == currentPage)
                footer = footer + "<li class='active'><a >" + (i + startIndex) + "</a></li>";
            else
                footer = footer + "<li><a href='javascript:void(0);' class='page " + PagePrefix + "' id=" + (i + startIndex) + ">" + (i + startIndex) + "</a></li>";
            if (parseInt(totalPages) == (i + startIndex))
                break;
        }
        if (parseInt(currentPage) < parseInt(totalPages)) {
            footer = footer + "<li class='next-btn'><a href='javascript:void(0)' title='Next' class='page " + PagePrefix + "' id=" + (parseInt(currentPage) + 1) + "><i class='fa fa-chevron-right' aria-hidden='true'></i></a></li>"
        }
        footer = footer + "</ul>"
        footer = footer;
        if (totalPages > 1) {
            $("." + footerContainer).html('');
            $("." + footerContainer).append(footer);
        }
        $("." + pagesizeContainer).html('');
        $("." + pagesizeContainer).append(pgSizeFooter);
        $(".rowsPerPage").val(rowsPerPage)
        $("a.page." + PagePrefix).click(function () {
            pageNumber = $(this).attr("id");
            {

                $("#" + ActionTypeID).val("pagechange");
                $("#" + CurrentPageID).val(pageNumber);

                fnCallback();
            }
        });

        $(".rowsPerPage").change(function ()
        {
            $("#" + CurrentPageID).val("1");
            $("#" + PageSizeID).val($(".rowsPerPage").val());
            fnCallback();
        })

    }
}

function setSortClass(sortByID, sortOrderID) {
    var previousSortBy = $("#" + sortByID).val();
    var sortOrder = $("#" + sortOrderID).val();
    $("a.sort").each(function () {
        var sortby = ($(this).attr("sortby"));
        if (sortby == previousSortBy) {
            if (sortOrder == '0') {
                $(this).removeClass("desc").addClass("asc");
            }
            else {
                $(this).removeClass("asc").addClass("desc");
            }
        }
    });
}


/* Using these functions for paging(example)

$().ready(function () {
        var msgType = $("#hdnMessageType1").val();
        var msg = $("#hdnMessage1").val();
        if (msg != '' && msgType != '') {
            showMessagePopup("divMessage", msgType, msg);
            $("#hdnMessage1").val("")
            $("#hdnMessageType1").val("");
        }
        $("a.sort").click(function () { bindSort(this, "SortBy1", "SortOrder1", "ActionType1", RefreshReportList) });
        createFooter("pg_pager1", parseInt($("#TotalPages1").val()), parseInt($("#PageSize1").val()), "ActionType1", "CurrentPage1", RefreshReportList);

    });


    function RefreshReportList() {
        beginRequest();
        var CPage = $("#CurrentPage1").val();
        var PSize = $("#PageSize1").val();
        var AType = $("#ActionType1").val();
        var SOrder = $("#SortOrder1").val();
        var SBy = $("#SortBy1").val();       

        $.ajax({
            url: '/Home/EurekaReportList',
            type: 'GET',
            data: { PageSize: PSize, SortBy: SBy, SortOrder: SOrder, CurrentPage: CPage },
            success: function (data) {
                endRequest();
                $("#dvReportList").html(data);

            },
            error: function (xhr, status, error) {
                endRequest();
                if (xhr.status != 403) {
                    // alert("An error occurred while loading the results.");
                }
            }
        });
    }

    // Pager Structure

    <div class="pagination-strip clearfix">
                    <div class="show-page-number">
                    </div>
                    <div id="" class=" pg_pager1 text-right pagination-wrp">
                    </div>
    </div>

*/