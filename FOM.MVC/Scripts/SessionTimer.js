﻿/*-----------------------Session timeout popup ------------------------------------------Added by Rakesh */
$(document).ready(function () {
    initSession();
});
var _timeLeft = parseInt($("#hdnTimerLeftStart").val());
var sess_pollInterval = parseInt($("#hdnWarningMsgBeforeTimeoutInMin").val());;
var sess_expirationMinutes = parseInt($("#hdnSessionTimeOut").val());
var sess_warningMinutes = parseInt($("#hdnWarningMsgBeforeTimeoutInMin").val());
var sess_intervalID;
// var sess_timeoutintervalID;
function initSession() {

    sessClearInterval();
    sessSetInterval();
    $(document).bind('keypress.session', function (ed, e) {
        sessKeyPressed(ed, e);
    });
    $(document).bind('mousemove.session', function (ed, e) {
        sessmousemoved(ed, e);
    });
}
function sessSetInterval() {
    //console.log('Start');
    sess_intervalID = setTimeout('sessInterval()', sess_pollInterval);
    //        sess_timeoutintervalID = setTimeout('sessLogOut()', sess_expirationMinutes);
}
function sessClearInterval() {
    clearTimeout(sess_intervalID);
    // clearTimeout(sess_timeoutintervalID);

}
function sessKeyPressed(ed, e) {
    sessClearInterval();
    sessSetInterval();
}
function sessmousemoved(ed, e) {
    sessClearInterval();
    sessSetInterval();
}
function sessLogOut() {
    javascript: document.getElementById('logoutForm').submit();
}
function sessInterval() {
    _timeLeft = parseInt($("#hdnTimerLeftStart").val());;
    $(document).unbind('keypress.session');
    $(document).unbind('mousemove.session');
    bootbox.dialog({
        message: "Your session will expire in <span id='CountDownHolder' style='color:Red;font-weight:bold;'> " + ((sess_expirationMinutes - sess_warningMinutes) / 60000) + "</span> Minute(s), press OK to remain logged in",
        closeButton: false,
        buttons: {
            Ok:
                           {
                               label: "Ok",
                               callback: function () {
                                   $(document).bind('keypress.session', function (ed, e) {
                                       sessKeyPressed(ed, e);
                                   });
                                   $(document).bind('mousemove.session', function (ed, e) {
                                       sessmousemoved(ed, e);
                                   });
                                   clearTimeout(_countDownTimer);
                                   initSession();
                               }

                           }
        }
    });
    updateCountDown();
}


var updateCountDown = function () {
    var min = Math.floor(_timeLeft / 60);
    var sec = _timeLeft % 60;
    if (sec < 10)
        sec = "0" + sec;
    if (document.getElementById("CountDownHolder") != null) {
        document.getElementById("CountDownHolder").innerHTML = min + ":" + sec;

        if (_timeLeft > 0) {
            _timeLeft--;
            _countDownTimer = window.setTimeout(updateCountDown, 1000);
        }
        else {
            sessLogOut();
        }
    }
    else {
        clearTimeout(_countDownTimer);
    }

};