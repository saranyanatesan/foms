﻿var dummyUi = {};
var itemArray = [];
function AutoComplete(txtid, actionUrl, parms, min, labelNoRecord, type, hdnId)
{
    min = 0;
    $('#' + txtid).autocomplete({
        minLength: 0,
        autoFocus: true,
        source: function (request, response) {            
            $("#" + hdnId).val("");
            ResetFields(type);
            var sendData = {};
            if ($("#" + txtid).val().length == 0) {
                response($.map(sendData, function (item) {
                    singleItem = {};                   
                    return singleItem;
                }))
            }
            else {
                $.each(parms, function (key, value) {
                    var k = key;
                    sendData[k] = $("#" + value).val();

                });
                $.ajax({
                    url: actionUrl,
                    data: sendData,
                    cache: false,
                    type: "POST",
                    success: function (data) {
                        itemArray = [];
                        response($.map(data, function (item) {
                            ResetFields(type);
                            singleItem = GetResponse(item, type);
                            itemArray.push(singleItem);
                            return singleItem;
                        }));

                    },
                    error: function (xhr, status, error) {
                        ResetFields(type);
                    }
                });
            }
        },
        select: function (event, ui) {            
            SetResponse(ui, txtid,type);
            return false;
        },
        response: function (event, ui) {
            if (!ui.content.length) {
                if ($("#" + txtid).val().length != 0)
                {
                    var noResult = { Value: "", label: labelNoRecord };
                    ui.content.push(noResult);
                }              
                   
                }
        },
        
        change: function (event, ui) {
         
            if($("#" +txtid).val().length < parseInt(min)){
                ResetResponse(type);
            }
            
        },
        search: function (event, ui)
        {
           
            if ($("#" + txtid).val().length < parseInt(min)) {
                return false;
            }
            $("#" + hdnId).val(ui.item ? ui.item.Id : "");
            
        },
        //close: function (event, ui) {
        //    alert("close");
            
        //}


    });


    $('#' + txtid).on("blur", function () {
        if ($("#" + txtid).val().length > parseInt(min)) {
            $.each(itemArray, function (i,item) {               
                if ($("#" + txtid).val().toLowerCase() == item.label.toLowerCase()) {
                    dummyUi.item = item;
                    SetResponse(dummyUi, txtid, type);
                }
            })
            
        }
    });

}

