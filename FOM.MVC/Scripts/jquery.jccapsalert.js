﻿var isCapsOn = false;
var iskeyPressed = false;
jQuery.fn.CapsLockAlert = function (options) {
    var settings = jQuery.extend({
        spanClass: "",
        delayIn: 0,      // delay before showing tooltip (ms)
        delayOut: 0,     // delay before hiding tooltip (ms)
        fade: false,     // fade tooltips in/out?
        fallback: 'CAPS Lock is on',    // fallback text to use when no tooltip text
        gravity: 's',    // gravity
        html: false,     // is tooltip content HTML?
        live: false,     // use live event support?
        offset: 10,       // pixel offset of tooltip from element
        opacity: 1.0,    // opacity of tooltip
        title: 'title',  // attribute/callback containing tooltip text
        trigger: 'manual', // how tooltip is triggered - hover | focus | manual
        stylize: true
        
    }, options);
    //return this.each(function() {
    jQuery(this).keypress(function (e) {
        //debugger;
        //   jQuery(this).tipsy(settings);
        iskeyPressed = true;
        var is_shift_pressed = false;
        if (e.shiftKey) {
            is_shift_pressed = e.shiftKey;
        }
        else if (e.modifiers) {
            is_shift_pressed = !!(e.modifiers & 4);
        }
        isCapsOn = is_shift_pressed;

        if (((e.which >= 65 && e.which <= 90) && !is_shift_pressed) || ((e.which >= 97 && e.which <= 122) && is_shift_pressed)) {
            isCapsOn = true;
            $("." + settings.spanClass).show();
            if (settings.stylize) {
                // todo
            }
        }
        else {
            isCapsOn = false;
            $("." + settings.spanClass).hide();
            // jQuery(this).tipsy("hide");
        }
    });

    jQuery(this).keyup(function (e) {

        if (jQuery(this).val() == "") {
            $("." + settings.spanClass).hide();
            //jQuery(this).tipsy("hide");		
        }

    });
    jQuery(this).keydown(function (e) {
        if (e.which == 20 && jQuery(this).val() != "") {
            //alert("IsCapsOn:" + isCapsOn)
            //alert("iskeyPressed:" + iskeyPressed)
            if (iskeyPressed) {
                if (isCapsOn) {
                    isCapsOn = false;
                    // jQuery(this).tipsy("show");
                    $("." + settings.spanClass).hide();
                }
                else {
                    isCapsOn = true;
                    // jQuery(this).tipsy("hide");
                    $("." + settings.spanClass).show();
                }
            }
        }
        else {
        }
    });
    //});
}