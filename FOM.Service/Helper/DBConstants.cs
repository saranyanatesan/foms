﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FOM.Service
{
    /// <summary>
    /// 
    /// </summary>
    public class DBConstants
    {
        #region Common
        #endregion

        #region JK Controls
        public const string admin_UserLogin = "admin_UserLogin";

        #region User 
        public const string admin_UserSearchList = "admin_UserSearchList";
        public const string admin_UserDelete = "admin_UserDelete";
        #endregion
        #endregion


        #region Client
        public const string admin_GetCustomerList = "admin_GetCustomerList";
        #endregion

        #region Employee
        public const string admin_GetEmployeeList = "admin_GetEmployeeList";
        #endregion
    }
}
