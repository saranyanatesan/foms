﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using FOM.ViewModels;
using FOM.Resources;

namespace FOM.Service
{
   public  class JobService :BaseService, IJobService
    {
        public ViewJobListModel JobList(ViewJobListModel viewJobListModel)
        {
            viewJobListModel.jobList = new List<JobModel>();

            SqlParameter[] parmList = {
                                      new SqlParameter("@RegionFranchiseMappingId",viewJobListModel.RegionFranchiseMappingId),
                                      new SqlParameter("@IsActive",viewJobListModel.IsEnable),
                                      new SqlParameter("@PageNo",viewJobListModel.CurrentPage),
                                      new SqlParameter("@PageSize",viewJobListModel.PageSize),
                                      new SqlParameter("@SortColumn",viewJobListModel.SortBy),
                                      new SqlParameter("@SortOrder",viewJobListModel.SortOrder),
                                     };

            using (DataSet ds = SQLHelper.ExecuteDataset(SQLHelper.ConnectionStringTransaction, CommandType.StoredProcedure, DBConstants.admin_GetEmployeeList, parmList))
            {
                if (ds != null && ds.Tables.Count > 0)
                {
                    //common function for Row to Entity GetDataRowToEntity<EntityModel>(ds.Table[0].Rows[0])     
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        viewJobListModel.jobList.Add(GetDataRowToEntity<JobModel>(dr));
                        viewJobListModel.TotalCount= Convert.ToInt32(ds.Tables[0].Rows[0]["TotalRecords"]);
                    }
                }

                if (viewJobListModel != null && viewJobListModel.jobList != null && viewJobListModel.jobList.Count > 0)
                {
                    int totalRecord = viewJobListModel.TotalCount;
                    if (decimal.Remainder(totalRecord, viewJobListModel.PageSize) > 0)
                        viewJobListModel.TotalPages = (totalRecord / viewJobListModel.PageSize + 1);
                    else
                        viewJobListModel.TotalPages = totalRecord / viewJobListModel.PageSize;
                }
                else
                {
                    viewJobListModel.TotalPages = 0;
                }


                return viewJobListModel;
            }
        }

        
    }

    public interface IJobService
    {
        ViewJobListModel JobList(ViewJobListModel viewJobListModel);
    }
}
