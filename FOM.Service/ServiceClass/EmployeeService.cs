﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using FOM.ViewModels;
using FOM.Resources;

namespace FOM.Service
{
   public  class EmployeeService : BaseService, IEmployeeService
    {
        public ViewEmployeeListModel EmployeeList(ViewEmployeeListModel viewClientListModel)
        {
            viewClientListModel.employeeList = new List<EmployeeListViewModel>();

            SqlParameter[] parmList = {
                                      new SqlParameter("@RegionFranchiseMappingId",viewClientListModel.RegionFranchiseMappingId),
                                      new SqlParameter("@IsActive",viewClientListModel.IsEnable),
                                      new SqlParameter("@PageNo",viewClientListModel.CurrentPage),
                                      new SqlParameter("@PageSize",viewClientListModel.PageSize),
                                      new SqlParameter("@SortColumn",viewClientListModel.SortBy),
                                      new SqlParameter("@SortOrder",viewClientListModel.SortOrder),
                                     };

            using (DataSet ds = SQLHelper.ExecuteDataset(SQLHelper.ConnectionStringTransaction, CommandType.StoredProcedure, DBConstants.admin_GetEmployeeList, parmList))
            {
                if (ds != null && ds.Tables.Count > 0)
                {
                    //common function for Row to Entity GetDataRowToEntity<EntityModel>(ds.Table[0].Rows[0])     
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        viewClientListModel.employeeList.Add(GetDataRowToEntity<EmployeeListViewModel>(dr));
                        viewClientListModel.TotalCount = Convert.ToInt32(ds.Tables[0].Rows[0]["TotalRecords"]);
                    }
                }

                if (viewClientListModel != null && viewClientListModel.employeeList != null && viewClientListModel.employeeList.Count > 0)
                {
                    int totalRecord = viewClientListModel.TotalCount;
                    if (decimal.Remainder(totalRecord, viewClientListModel.PageSize) > 0)
                        viewClientListModel.TotalPages = (totalRecord / viewClientListModel.PageSize + 1);
                    else
                        viewClientListModel.TotalPages = totalRecord / viewClientListModel.PageSize;
                }
                else
                {
                    viewClientListModel.TotalPages = 0;
                }


                return viewClientListModel;
            }
        }
    }

    public interface IEmployeeService
    {
        ViewEmployeeListModel EmployeeList(ViewEmployeeListModel viewClientListModel);
    }
}
