﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using FOM.ViewModels;
using FOM.Resources;

namespace FOM.Service
{
    public class UserService : BaseService, IUserService
    {
        public UserViewModel Login(UserLoginViewModel loginViewModel)
        {
            UserViewModel userViewModel = new UserViewModel();
            int ErrorCode = 0;
            string ErrorMessage = "";

            SqlParameter pErrorCode = new SqlParameter("@ErrorCode", ErrorCode);
            pErrorCode.Direction = ParameterDirection.Output;
            SqlParameter pErrorMessage = new SqlParameter("@ErrorMessage", ErrorMessage);
            pErrorMessage.Direction = ParameterDirection.Output;
            pErrorMessage.Size = 8000;

            SqlParameter[] parmList = {
                new SqlParameter("@Username",loginViewModel.Username.Trim()),
                new SqlParameter("@Password",loginViewModel.Password.Trim()),
                new SqlParameter("@IPAddress",loginViewModel.IPAddress),
                pErrorCode,
                pErrorMessage
            };

            //Verify user details from database
            using (DataSet userDetailsDS = SQLHelper.ExecuteDataset(SQLHelper.ConnectionStringTransaction, CommandType.StoredProcedure, DBConstants.admin_UserLogin, parmList))
            {

                if (userDetailsDS != null && userDetailsDS.Tables != null && userDetailsDS.Tables.Count > 2)
                {
                    DataTable userRoleMappingDT = userDetailsDS.Tables[1];
                    DataTable userClientMappingDT = userDetailsDS.Tables.Count > 2 ? userDetailsDS.Tables[2] : null;

                    //User Details 
                    if (userDetailsDS.Tables[0].Rows != null && userDetailsDS.Tables[0].Rows.Count > 0)
                    {
                        userViewModel = GetDataRowToEntity<UserViewModel>(userDetailsDS.Tables[0].Rows[0]);
                    }

                    //Role List with All Details 
                    if (userDetailsDS.Tables[1].Rows != null && userDetailsDS.Tables[1].Rows.Count > 0)
                    {
                        userViewModel.Roles = new List<RoleModel>();
                        foreach (DataRow item in userDetailsDS.Tables[1].Rows)
                        {
                            userViewModel.Roles.Add(GetDataRowToEntity<RoleModel>(item));
                        }
                    }

                    //Company List with All Details 
                    if (userDetailsDS.Tables[2].Rows != null && userDetailsDS.Tables[2].Rows.Count > 0)
                    {
                        userViewModel.Regions = new List<RegionInfoViewModel>();
                        foreach (DataRow item in userDetailsDS.Tables[2].Rows)
                        {
                            userViewModel.Regions.Add(GetDataRowToEntity<RegionInfoViewModel>(item));
                        }
                    }
                }
                else if (Convert.ToInt32(pErrorCode.Value) > 0)
                    userViewModel.objErrorModel.Add(new ErrorModel() { ErrorCode = Convert.ToInt32(pErrorCode.Value), ErrorMessage = Convert.ToString(pErrorMessage.Value) });
                else
                    userViewModel.objErrorModel.Add(new ErrorModel() { ErrorCode = 0, ErrorMessage = string.Format(CommonResource.msgCommonError, "{0}", "login") });
            }
            return userViewModel;
        }
 
    }

    public interface IUserService
    {
        UserViewModel Login(UserLoginViewModel loginViewModel);
    }
}
