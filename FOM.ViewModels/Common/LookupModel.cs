﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FOM.ViewModels
{
    public class LookupModel : BaseModel
    {
        public string Key { get; set; }
        public int? FilterId { get; set; }
        public int Id { get; set; }
        public string Name { get; set; }
        public int DisplayCount { get; set; }
    }



    public class LookupListViewModel : BaseModel
    {
        public LookupListViewModel()
        {
            LookupList = new List<LookupModel>();
        }
        public List<LookupModel> LookupList { get; set; }
    }

    /// <summary>
    /// this Model is used to get max display order by Key.
    /// </summary>
    public class MaxDisplayOrderModel : BaseModel
    {
        public int MaxDisplayOrder { get; set; }
    }
}
