﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FOM.ViewModels
{
    public class CustomerModel : BaseModel
    {
      
        public CustomerModel()
        {
            Address = new AddressModel();
        }

        public int CustomerId { get; set; }
        public string CustomerCode { get; set; }
        public string Name { get; set; }
        public string EmailId { get; set; }
        public string Phone { get; set; }
        public string Fax { get; set; }
        public int? AddressId { get; set; }
        
        public AddressModel Address { get; set; }
        
    }

    public class ViewCustomerListModel : PaggingModel
    {
        public ViewCustomerListModel()
        {
            customerList = new List<CustomerModel>();
        }

        public string SearchText { get; set; }

        public int ActiveStatus { get; set; }

        public List<CustomerModel> customerList { get; set; }
    }
}
