﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FOM.ViewModels
{
    public class AddressModel : BaseModel
    {
      
        public AddressModel()
        {

        }

        public int AddressId { get; set; }
       
        public Nullable<int> AddressTypeId { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Country { get; set; }
        public string ZipCode { get; set; }
        public Nullable<decimal> Latitude { get; set; }
        public Nullable<decimal> Longitude { get; set; }
        public string GoogleMapUrl { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        
    }

    public class AddressTypeModel : BaseModel
    {
        public AddressTypeModel()
        {
            
        }

        public int AddressTypeId { get; set; }
        public Nullable<int> Type { get; set; }
        public string Name { get; set; }

    }
}
