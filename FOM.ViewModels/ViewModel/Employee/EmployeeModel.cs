﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FOM.ViewModels
{
    public class EmployeeModel : BaseModel
    {
        public EmployeeModel()
        {
            Address = new AddressModel();
        }

        public int EmpId { get; set; }

        public int RegionFranchiseMappingId { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string MiddleInitial { get; set; }

        public int? PictureId { get; set; }

        public string WorkPhone { get; set; }

        public string MobilePhone { get; set; }

        public string HomePhone { get; set; }

        public DateTime? DateOfBirth { get; set; }

        public string Notes { get; set; }

        public int? UserId { get; set; }

        public int? AddressId { get; set; }

        public bool? EnablePushNotification { get; set; }

        public bool? EnableEmailNotification { get; set; }

        public AddressModel Address { get; set; }
    }

    public class EmployeeListViewModel
    {
        public int EmpId { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string AddressLine1 { get; set; }

        public int UserId { get; set; }

        public string UserName { get; set; }

        public string Email { get; set; }

    }

    public class ViewEmployeeListModel : PaggingModel
    {
        public ViewEmployeeListModel()
        {
            employeeList = new List<EmployeeListViewModel>();
        }

        public string SearchText { get; set; }

        public int ActiveStatus { get; set; }

        public List<EmployeeListViewModel> employeeList { get; set; }
    }
}
