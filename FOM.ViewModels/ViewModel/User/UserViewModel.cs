﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FOM.ViewModels
{
    /// <summary>
    /// CMS user model
    /// </summary>
    public class UserViewModel : BaseModel
    {
        public UserViewModel()
        {
            Roles = new List<RoleModel>();
            Regions = new List<RegionInfoViewModel>();           
        } 
        public List<RoleModel> Roles { get; set; }
        public List<RegionInfoViewModel> Regions { get; set; }
        
        public string RoleIds { get; set; }
        
        public string RoleName { get; set; }
        public int RoleId { get; set; }

        public int UserId { get; set; }

        public string UserName { get; set; }

        public string PasswordHash { get; set; }

        public DateTime? IsFirstTimeLogin { get; set; }

        public Guid Salt { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Email { get; set; }

        public string Phone { get; set; }

        public string Addres { get; set; }

        public string City { get; set; }

        public string State { get; set; }

        public string Zipcode { get; set; }

    }
 
    public class UserViewListModel : PaggingModel
    {
        public UserViewListModel()
        {
            userListModel = new List<UserViewModel>();
        }

        public string SelectedUserId { get; set; }

        public List<UserViewModel> userListModel { get; set; }
    }

    public class RoleModel : BaseModel
    {
        public int RoleId { get; set; }
        public string RoleName { get; set; }
        public string RoleType { get; set; }
        public int RoleTypeId { get; set; }
        public int CompanyId { get; set; }
    }

    public class RoleListModel : BaseModel
    {
        public List<RoleModel> lstRole { get; set; }
    }


    public class RoleTypeModel : BaseModel
    {
        public int RoleTypeId { get; set; }
        public string RoleType { get; set; }
    }

    public class RegionInfoViewModel
    {
        public int RegionId { get; set; }

        public int RegionName { get; set; }
 
    }
}
