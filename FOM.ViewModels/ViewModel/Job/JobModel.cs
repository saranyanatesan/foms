﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FOM.ViewModels
{
   public class JobModel
    {

        public int Job_Code { get; set; }
        public string Date { get; set; }
        public string Title { get; set; }
        public string Ship { get; set; }
        public string Price { get; set; }
        public string Amount { get; set; }
        public string Status { get; set; }
        public string Actions { get; set; }

        
    }
    public class ViewJobListModel : PaggingModel
    {
        public ViewJobListModel()
        {
            jobList = new List<JobModel>();
        }

        public string SearchText { get; set; }

        public int ActiveStatus { get; set; }

        public List<JobModel> jobList { get; set; }
    }
}
