﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FOM.ViewModels
{
    public enum VMenuEnum
    {
        Dashboard,
        Job,
        Customer,
        Employee,
        Schedule,
        Map,
        Report,
        Other,
        FranchisePortal
    }

    public enum HMenuEnum
    {
        Dashboard,
        Job,
        Client,
        Employee,
        Schedule,
        Map,
        Report,
        Other
    }
}
