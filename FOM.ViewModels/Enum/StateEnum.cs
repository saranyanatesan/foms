﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FOM.ViewModels
{
    public enum OrderStateEnum
    {
        In_Progress,
        Scheduled,
        Complete,
        Closed,
        Reopen
    }

    public enum ExtraWorkStateEnum
    {
        Requested,
        Approved,
        Complete,
        Closed
    }
}
